package com.example.example.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.example.R
import com.example.example.model.BoardData
import kotlinx.android.synthetic.main.pageritem.view.*

//Класс-адаптер для viewpager в OnBoarding
class PagerAdapter(val list : List<BoardData>) : RecyclerView.Adapter<PagerAdapter.VH>() {
    //Класс с описанием шаблона элемента OnBoarding
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val quote = itemView.quote
        val image = itemView.imageView
    }

    //Создаем новый шаблон
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.pageritem, parent, false))
    }

    //Заполняем шаблон на основе массива, который мы передали
    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.image.setImageResource(list[position].image)
        holder.quote.text = list[position].quote
        holder.image.rotation = list[position].rotation
    }

    //Возвращаем кол-во экранов
    override fun getItemCount(): Int {
        return list.size
    }
}