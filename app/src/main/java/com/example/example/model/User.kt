package com.example.example.model

//Информация о пользователе, которая приходит от сервера при авторизации или регистрации
data class User(
    val avatar: Any,
    val firstname: String,
    val id: Int,
    val lastname: String,
    val patronymic: String
)