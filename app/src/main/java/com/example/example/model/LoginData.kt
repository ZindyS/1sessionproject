package com.example.example.model

//Дата класс для запроса на авторизацию
data class LoginData(
    val email : String,
    val password : String
)
