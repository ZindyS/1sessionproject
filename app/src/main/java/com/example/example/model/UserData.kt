package com.example.example.model

//Информация, которая приходит от сервера при авторизации или регистрации
data class UserData(
    val token: String,
    val user: User
)