package com.example.example.model

//Дата класс для запроса на регистрацию
data class RegisterData(
    val dateBirthDay: String,
    val email: String,
    val firstname: String,
    val lastname: String,
    val password: String,
    val patronymic: String,
    val sex: String
)