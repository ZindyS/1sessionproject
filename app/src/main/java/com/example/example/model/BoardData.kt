package com.example.example.model

//Дата класс для OnBoarding
data class BoardData(
    val quote : String,
    val rotation : Float,
    val image : Int
)
