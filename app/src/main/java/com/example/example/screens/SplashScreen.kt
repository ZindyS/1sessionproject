package com.example.example.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.example.example.common.CodemaniaAPI
import com.example.example.R
import com.example.example.common.Utils
import com.example.example.model.LoginData
import com.example.example.model.UserData
import kotlinx.android.synthetic.main.activity_splash_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//Класс описания экрана Splash
class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        //Задержка
        Handler().postDelayed({
            //Отображение прогресса
            progressBar.visibility = View.VISIBLE

            val sh = Utils.getSharedPreferences(this)

            //Подготовка запроса
            val retrofit = Retrofit.Builder()
                .baseUrl("http://strukov-artemii.online:8085")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val api = retrofit.create(CodemaniaAPI::class.java)

            //Запрос
            api.signIn(LoginData(sh.getString("email", "")!!, sh.getString("password", "")!!))
                .enqueue(object : Callback<UserData> {
                    override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                        if (response.isSuccessful) {
                            startActivity(Intent(this@SplashScreen, MainActivity::class.java))
                            finish()
                        } else {
                            startActivity(Intent(this@SplashScreen, OnBoardingActivity::class.java))
                            finish()
                        }
                    }

                    override fun onFailure(call: Call<UserData>, t: Throwable) {
                        startActivity(Intent(this@SplashScreen, OnBoardingActivity::class.java))
                        finish()
                    }
                })
        }, 1000)
    }
}