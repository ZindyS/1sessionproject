package com.example.example.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.example.adapters.PagerAdapter
import com.example.example.R
import com.example.example.model.BoardData
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_on_boarding.*

//Класс описания экрана OnBoarding
class OnBoardingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        //Создаём список элементов для viewpager'а
        val list = listOf(
            BoardData("Без теории, только практика\n" +
                "Вы не платите за лекции и теоретический материал, ведь все это можно найти в интернете бесплатно",
                0f, R.drawable.first
            ), BoardData("Без теории, только практика\n" +
                "Вы не платите за лекции и теоретический материал, ведь все это можно найти в интернете бесплатно",
            -4.76f, R.drawable.second
            ), BoardData("Обучение онлайн из любой точки мира\n" +
                "Наше обучение изначально создавалось как дистанционное", 5.5f, R.drawable.third
            )
        )
        //Создаём адаптер
        pager.adapter = PagerAdapter(list)
        //Отключаем возможность управдения свайпом
        pager.isUserInputEnabled = false
        //Добавление слушателя для перелистывания чтобы изменить текст кнопки (можно было сделать
        //обычной переменной)
        pager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position==2) {
                    button3.text = "Начать"
                }
            }
        })
        //Добавление индикатора прогресса
        TabLayoutMediator(tabLayout, pager, true) { tab: TabLayout.Tab, i: Int -> }.attach()
    }

    //При нажатии на кнопку Далее
    fun onButtonClicked(v: View) {
        //Проверка на то, какой экран сейчас открыт
        if (pager.currentItem==2) {
            //Переход на след. экран
            startActivity(Intent(this, SignInActivity::class.java))
            finish()
        } else {
            //Перелистывание
            pager.currentItem++
        }
    }
}