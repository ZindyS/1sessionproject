package com.example.example.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.text.isDigitsOnly
import com.example.example.common.CodemaniaAPI
import com.example.example.R
import com.example.example.common.Utils
import com.example.example.model.RegisterData
import com.example.example.model.UserData
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_sign_in.editTextTextPassword
import kotlinx.android.synthetic.main.activity_sign_in.editTextTextPersonName
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//Класс описания экрана регистрации
class RegisterActivity : AppCompatActivity() {
    var sex = "Мужчина"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        //Добавляем слушателя для выпадающего списка
        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2==0) {
                    sex = "Мужчина"
                } else {
                    sex = "Женщина"
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }
    }

    //При нажатии на кнопку регистрации
    fun onRegisterClicked(v: View) {
        //Проверка
        if (editTextTextPersonName.text.toString()=="" || editTextTextPersonName2.text.toString()=="" ||
            editTextTextPersonName3.text.toString()=="" || editTextTextPersonName4.text.toString()=="" ||
            editTextTextPersonName5.text.toString()=="" || editTextTextPassword2.text.toString()=="" ||
                editTextTextPassword.text.toString()=="") {
            Utils.showAlertDialog(this, "Ошибка", "Проверьте поля на пустоту", "Попробовать снова")
            return
        }

        if (editTextTextPassword.text.toString() != editTextTextPassword2.text.toString()) {
            Utils.showAlertDialog(this, "Ошибка", "Ваши пароли не совпадают", "Попробовать снова")
            return
        }

        if (!Utils.validateEmail(editTextTextPersonName.text.toString())) {
            Utils.showAlertDialog(
                this,
                "Ошибка",
                "Проверьте правильность вашего email",
                "Попробовать снова"
            )
            return
        }

        //Форматирование даты
        var date = editTextTextPersonName5.text.toString().split(' ')
        val months = listOf("января", "февраля", "марта", "апреля", "мая", "июня", "июля",
            "августа", "сентября", "октября", "ноября", "декабря")

        if (date.size!=3 || !date[0].isDigitsOnly() || !date[2].isDigitsOnly() || !months.contains(date[1]) || date[0].toInt()>31) {
            Utils.showAlertDialog(
                this,
                "Ошибка",
                "Проверьте правильность вашей даты рождения",
                "Попробовать снова"
            )
            return
        }

        var day = date[0]
        if (date[0].length==1)
            day = "0$day"
        var month = (months.indexOf(date[1])+1).toString()
        if (month.length==1)
            month = "0$month"
        val sdate = "$day.$month.${date[2]}"
        Log.d("errror", sdate)

        //Подготовка запроса
        val retrofit = Retrofit.Builder()
            .baseUrl("http://strukov-artemii.online:8085")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(CodemaniaAPI::class.java)

        //Запрос
        api.signUp(
            RegisterData(sdate, editTextTextPersonName.text.toString(),
            editTextTextPersonName3.text.toString(), editTextTextPersonName2.text.toString(), editTextTextPassword.text.toString(),
            editTextTextPersonName4.text.toString(), sex)
        )
            .enqueue(object : Callback<UserData> {
                override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                    if (response.isSuccessful) {
                        //Сохранение данных
                        val ed = Utils.getSharedPreferences(this@RegisterActivity).edit()
                        ed.putString("token", response.body()!!.token)
                        ed.putString("email", editTextTextPersonName.text.toString())
                        ed.putString("password", editTextTextPassword.text.toString())
                        ed.commit()
                        startActivity(Intent(this@RegisterActivity, MainActivity::class.java))
                    } else {
                        Utils.showAlertDialog(
                            this@RegisterActivity,
                            "Ошибка",
                            "Такой пользователь уже существует",
                            "Попробовать снова"
                        )
                    }
                }

                override fun onFailure(call: Call<UserData>, t: Throwable) {
                    Utils.showAlertDialog(
                        this@RegisterActivity,
                        "Ошибка",
                        t.message!!,
                        "Попробовать снова"
                    )
                }
            })
    }

    //При нажатии на кнопку Назад
    fun onBackClicked(v: View) {
        finish()
    }
}