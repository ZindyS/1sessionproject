package com.example.example.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.example.common.CodemaniaAPI
import com.example.example.R
import com.example.example.common.Utils
import com.example.example.model.LoginData
import com.example.example.model.UserData
import kotlinx.android.synthetic.main.activity_sign_in.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//Экран описания экрана с авторизацией
class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
    }

    //При нажатии на авторизацию
    fun onSignInClicked(v: View) {
        //Проверка
        if (editTextTextPassword.text.toString() == "" || editTextTextPersonName.text.toString() == "") {
            Utils.showAlertDialog(this, "Ошибка", "Проверьте поля на пустоту", "Попробовать снова")
            return
        }

        if (!Utils.validateEmail(editTextTextPersonName.text.toString())) {
            Utils.showAlertDialog(
                this,
                "Ошибка",
                "Проверьте правильность вашего email",
                "Попробовать снова"
            )
            return
        }

        //Подготовка к запросу
        val retrofit = Retrofit.Builder()
            .baseUrl("http://strukov-artemii.online:8085")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(CodemaniaAPI::class.java)

        //Запрос
        api.signIn(LoginData(editTextTextPersonName.text.toString(), editTextTextPassword.text.toString()))
            .enqueue(object : Callback<UserData> {
                override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                    if (response.isSuccessful) {
                        //Сохранение данных
                        val ed = Utils.getSharedPreferences(this@SignInActivity).edit()
                        ed.putString("token", response.body()!!.token)
                        ed.putString("email", editTextTextPersonName.text.toString())
                        ed.putString("password", editTextTextPassword.text.toString())
                        ed.commit()
                        startActivity(Intent(this@SignInActivity, MainActivity::class.java))
                    } else {
                        Utils.showAlertDialog(
                            this@SignInActivity,
                            "Ошибка",
                            "Проверьте логин и пароль",
                            "Попробовать снова"
                        )
                    }
                }

                override fun onFailure(call: Call<UserData>, t: Throwable) {
                    Utils.showAlertDialog(
                        this@SignInActivity,
                        "Ошибка",
                        t.message!!,
                        "Попробовать снова"
                    )
                }
            })
    }

    //При нажатии на кнопку регистрации
    fun onRegisterClicked(v: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }
}