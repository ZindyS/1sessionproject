package com.example.example.common

import com.example.example.model.LoginData
import com.example.example.model.RegisterData
import com.example.example.model.UserData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

//Класс с описанием запросов на сервер
interface CodemaniaAPI {
    @POST("signIn")
    fun signIn(@Body data : LoginData) : Call<UserData>

    @POST("signUp")
    fun signUp(@Body data : RegisterData) : Call<UserData>
}