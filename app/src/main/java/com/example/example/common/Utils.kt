package com.example.example.common

import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import androidx.appcompat.app.AlertDialog

//Обший класс со статичными методами
class Utils {
    companion object {
        //Проверка email'а
        fun validateEmail(email: String): Boolean {
            var cnt = -1
            var correct = false
            for (i in email) {
                if (cnt != -1)
                    cnt++

                if (i == '@') {
                    if (correct) {
                        correct = false
                        break
                    }
                    correct = true
                } else if (i == '.') {
                    cnt = 0
                } else if (i in 'a'..'z') {
                } else if (cnt == -1 && i in '0'..'9') {
                } else {
                    correct = false
                    break
                }
            }

            return correct && cnt<=3 && cnt>=1
        }

        //Показывает AlertDialog с нужными параметами
        fun showAlertDialog(context: Context, title : String, message : String, button : String) {
            AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(button) { dialogInterface: DialogInterface, i: Int -> }
                .show()
        }

        //Возвращает объект SharedPreferences с опред. параметрами
        fun getSharedPreferences(context: Context) : SharedPreferences {
            return context.getSharedPreferences("Example1", 0)
        }
    }
}